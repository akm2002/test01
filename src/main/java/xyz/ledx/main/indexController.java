package xyz.ledx.main;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class indexController {

    @RequestMapping("/")
    public String index() {
    	
    	Date d1 = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
    	
    	
        return "Hello ED-Ward</br>当前时间："+sdf.format(d1);
    }
    
    
    @RequestMapping("/hello")
    public String sayHello(String name) {
    	
    	Date d1 = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
    	
    	
        return "Hello "+name+"</br>当前时间："+sdf.format(d1);
    }
}
